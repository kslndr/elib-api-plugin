<?php

define('api_config', [
    'plugin_dir'        => dirname(plugin_basename(__FILE__)) . '/',
    'plugin_path'       => plugin_dir_path(__FILE__),
    'plugin_dir_url'    => plugin_dir_url(__FILE__),
    'api_url'           => 'https://elibrary.aes2.org/',
]);
