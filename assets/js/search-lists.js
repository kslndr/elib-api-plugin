jQuery(document).ready(function($){

    if ( $("#advanced-search").length || $('#refine-search').length ) {

        //var api_url = 'https://135.148.120.158:8000';
        var api_url = 'https://elibrary.aes2.org';
        
        $('.js-authors-multiple').select2({ 
            minimumInputLength: 3, 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-authors-js/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            },
        });

        $('.js-affiliation-multiple').select2({ 
            minimumInputLength: 3, 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-affiliation-js/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });

        $('.js-conference').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-elib-js/conference/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });
        $('.js-convention').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-elib-js/convnum/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });
        $('.js-engineering').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-elib-js/convnum/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });
        $('.js-jaesissue').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-elib-js/jaesissue/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });
        $('.js-jaesvolume').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-elib-js/jaesvolume/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });
        $('.js-doccdnum').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-elib-js/doccdnum/?term=aes',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });

        $('.js-audio-topic').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-for-file-form-js/collectionnames/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });
        $('.js-tags-multiple').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-for-file-form-js/tags/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });
        $('.js-keywords-multiple').select2({ 
            theme: "flat",
            width: '100%',
            ajax: {
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }
                    // Query parameters will be ?term=[term]&page=[page]
                    return query;
                },
                url: api_url+'/api/v1/list-for-file-form-js/keywords/',
                processResults: function (data, params) {
                    params.page = params.page || 1;
    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * data.page_size) < data.count_filtered
                        }
                    };
                }
            }
        });

    }

});