jQuery(document).ready(function($){

    if ( $("#advanced-search").length || $('#refine-search').length ) {

        $('#type_document').change(function() {
            console.log( $(this).val() );
            change_radiobutton ();
        });

        $("#conference").on('select2:select', function (e) {
            $("#limit_search_conference_papers").prop("checked", true); 
            $("#type_document").val("conference Paper").change();
            if ( $("#advanced-search").length )  change_radiobutton ("Conference Paper");
        });
        $("#convention").on('select2:select', function (e) {
            $("#limit_search_convention_papers").prop("checked", true);
            $("#type_document").val("Convention Paper").change();
            if ( $("#advanced-search").length )  change_radiobutton ("Convention Paper");
        });
        $("#engineering").on('select2:select', function (e) {
            $("#limit_search_engineering_briefs").prop("checked", true); 
            $("#type_document").val("Engineering Brief").change();
            if ( $("#advanced-search").length )  change_radiobutton ("Engineering Brief");
        });
        $("#jaesvolume").on('select2:select', function (e) {
            $("#limit_search_journal_articles").prop("checked", true);
            $("#type_document").val("Journal Article").change();
            if ( $("#advanced-search").length )  change_radiobutton ("Journal Article");
        });

        $("#doccdnum").on('select2:select', function (e) {
            $("#search_stantards").prop("checked", true);
        });
        $("#search_stantards").change(function() {
            $(".js-doccdnum").val(null).trigger('change');
        });


        $('input[type=radio][name=limit_search]').change(function() {
            if (this.value == '') {
                $("#type_document").val("").change();
                console.log("limit_search_all = "+this.value);
                if ( $("#advanced-search").length )  change_radiobutton ("");
            }
            if (this.value == 'conference_papers') {
                $("#type_document").val("conference Paper").change();
            }
            else if (this.value == 'convention_papers') {
                $("#type_document").val("Convention Paper").change();
            }
            else if (this.value == 'engineering_briefs') {
                $("#type_document").val("Engineering Brief").change();
            }
            else if (this.value == 'journal_articles') {
                $("#type_document").val("Journal Article").change();
            }
            console.log( "value = "+this.value );
        });

        function change_radiobutton (type = null) {
            if (type == null) type = $('#type_document option:selected').val();
            console.log( "type = "+type );

            if (type == "") {
               $("#limit_search_all").prop("checked", true); 
            } 
            if (type == "Conference Paper" || type == "conference Paper") {
               $("#limit_search_conference_papers").prop("checked", true); 
            } 
            if (type == "Convention Paper") {
                $("#limit_search_convention_papers").prop("checked", true);
            } 
            if (type == "Engineering Brief") {
               $("#limit_search_engineering_briefs").prop("checked", true); 
            } 
            if (type == "Journal Article") {
                $("#limit_search_journal_articles").prop("checked", true);
            }

        
            if (type != "Conference Paper" && type != "conference Paper") {
                $(".js-conference").val(null).trigger('change');
            } 
            if (type != "Convention Paper") {
               $(".js-convention").val(null).trigger('change');
            } 
            if (type != "Engineering Brief") {
                $(".js-engineering").val(null).trigger('change');
            }
            
            if (type != "Journal Article") {
                $(".js-jaesvolume").val(null).trigger('change');
            }
        }

    }

});