<?php
/**
  Plugin Name: Elib Api plugin for Wordpress
  Plugin URI: 
  Description: The plugin receives data from the API.
  Version: 1.0.0
  Author: Elib
  Author URI: https://www.aes.org/
  Text Domain: ApiPlugin
  Domain Path: /languages
 */

defined( 'ABSPATH' ) || exit;

require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

try {
  (new Api\ApiPlugin([]))->run();
} catch (\Exception $ex) {
  echo 'Error: ',  $ex->getMessage(), "\n";
}