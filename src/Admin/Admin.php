<?php

namespace Api\Admin;
use Api\Includes\Functions;

defined( 'ABSPATH' ) || exit;

/**
 * The main class for working with the admin panel
 */
class Admin {

    /**
     * Register the scripts for the public-facing side of the site.
     *
     * @return void
     */
    public function enqueue_scripts() {
        // wp_enqueue_script('api-admin-script', Functions::getConfigOption('plugin_dir_url') . 'assets/js/api-admin.js', ["jquery"], time());
        // wp_enqueue_style('api-style', Functions::getConfigOption('plugin_dir_url') . 'assets/css/api-style.css', [], time());

    }

}
