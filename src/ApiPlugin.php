<?php
namespace Api;

use Api\Includes\Loader;
use Api\Admin\Admin;
use Api\Frontend\Frontend;

defined( 'ABSPATH' ) || exit;

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 */
class ApiPlugin {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @var object
     */
    private $loader;

    /**
     * Define the core functionality of the plugin.
     */
    public function __construct() {
        $this->loader = new Loader;
        $this->define_admin_hooks();
        $this->define_frontend_hooks();
        $this->define_frontend_filtres();
        $this->define_frontend_shortcodes();
    }


    /**
     * Register all of the hooks related to the admin area functionality of the plugin.
     *
     * @return void
     */
    private function define_admin_hooks() {
        $admin = new Admin();
        $this->loader->add_action('admin_enqueue_scripts', $admin, 'enqueue_scripts');
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @return void
     */
    private function define_frontend_hooks() {
        $frontend = new Frontend();
        $this->loader->add_action('init', $frontend, 'wp_init');

        $this->loader->add_action( 'admin_post_getDataFromApi', $frontend, 'getDataFromApi' );
        $this->loader->add_action( 'admin_post_nopriv_getDataFromApi', $frontend, 'getDataFromApi' );
        $this->loader->add_action( 'admin_post_formInst', $frontend, 'formInst' );
        $this->loader->add_action( 'admin_post_nopriv_formInst', $frontend, 'formInst' );
        $this->loader->add_action( 'wp_ajax_set-ip', $frontend, 'setIp'  );
        $this->loader->add_action( 'wp_ajax_nopriv_set-ip', $frontend, 'setIp' );

        $this->loader->add_action( 'wp_enqueue_scripts', $frontend, 'gd_enqueue');
        $this->loader->add_action( 'wp_ajax_nopriv_add_to_cart', $frontend, 'add_to_cart_callback' );
        $this->loader->add_action( 'wp_ajax_add_to_cart', $frontend, 'add_to_cart_callback' );

        // $this->loader->add_action('init', $frontend, 'do_rewrite_section', 10, 0);

        $this->loader->add_action('woocommerce_checkout_create_order_line_item', $frontend, 'save_cart_item_key_as_custom_order_item_metadata', 10, 4 );
        $this->loader->add_action( 'woocommerce_payment_complete', $frontend, 'action__woocommerce_payment_complete' );
    }

    /**
     * Register all of the filtres related to the public-facing functionality
     * of the plugin.
     *
     * @return void
     */
    private function define_frontend_filtres() {
        $frontend = new Frontend();
        $this->loader->add_filter( 'wp_mail_content_type', $frontend, 'set_mail_content_type' );
        $this->loader->add_filter( 'woocommerce_cart_item_permalink', $frontend, 'custom_product_images_link', 10, 3 );
        $this->loader->add_filter( 'woocommerce_cart_item_thumbnail', $frontend, 'custom_product_image', 10, 3 );	
        $this->loader->add_filter( 'woocommerce_is_sold_individually', $frontend, 'wc_remove_quantity_field_from_cart', 10, 2 );
        $this->loader->add_filter( 'woocommerce_cart_item_name', $frontend, 'change_cart_item_name', 10, 3);
    }

    /**
     * Register all of the shortcodes related to the public-facing functionality
     * of the plugin.
     *
     * @return void
     */
    public function define_frontend_shortcodes() {
        $frontend = new Frontend();
        $this->loader->add_shortcode('form_login_institutions', $frontend, 'fe_form_login_institutions');

        $this->loader->add_shortcode('type_to_search', $frontend, 'ttype_to_search');
        $this->loader->add_shortcode('type_to_search_bottom', $frontend, 'ttype_to_search_bottom');
        $this->loader->add_shortcode('power_search', $frontend, 'tpower_search_form');
        
        $this->loader->add_shortcode('publications_home', $frontend, 'elib_publications_home');

        $this->loader->add_shortcode('e_library_page', $frontend, 'fe_library_page');
        $this->loader->add_shortcode('jaes-journal', $frontend, 'fe_jaes_journal');
        $this->loader->add_shortcode('jaes-journal-online', $frontend, 'fe_jaes_journal_online');
        $this->loader->add_shortcode('jaes-journal-top', $frontend, 'fe_jaes_journal_top');

        $this->loader->add_shortcode('standards', $frontend, 'fe_shortcode_standards');
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @return void
     */
    public function run() {
        // Includes\Functions::saveReferrer();
        $this->loader->run();
    }
}
