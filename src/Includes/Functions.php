<?php
namespace Api\Includes;

defined( 'ABSPATH' ) || exit;

/**
 * Class for adding functions
 */
class Functions {

    /**
     * Returns plugin config option
     * @param string $option_name
     * @return string|array
     */
    public static function getConfigOption($option_name) {
        return api_config[$option_name];
    }

    /**
     * Get event id by event name
     * @param string $event_name
     * @return string
     */
    public static function eventIdByName($event_name) {
        foreach (Functions::getConfigOption('available_events') as $val) {
            if ($val['name'] == $event_name) {
                return $val['id'];
            }
        }
        return $event_name;
    }

    /**
     * Get browser name
     * @return string
     */
    public static function getBrowserName() {
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $t = strtolower($_SERVER['HTTP_USER_AGENT']);
            $t = " " . $t;
            if     (strpos($t, 'opera'     ) || strpos($t, 'opr/')     ) return 'Opera'            ;   
            elseif (strpos($t, 'edge'      )                           ) return 'Edge'             ;   
            elseif (strpos($t, 'chrome'    )                           ) return 'Chrome'           ;   
            elseif (strpos($t, 'safari'    )                           ) return 'Safari'           ;   
            elseif (strpos($t, 'firefox'   )                           ) return 'Firefox'          ;   
            elseif (strpos($t, 'msie'      ) || strpos($t, 'trident/7')) return 'Internet Explorer';
        }
        return 'Unkown';
    }

    /**
     * Get current country
     * @return type
     */
    public static function getCurrentCountry() {
        $getGeoIP = self::getGeoIP();
        return $getGeoIP['geoplugin_countryName'];
    }

    /**
     * Get current region
     * @return type
     */
    public static function getCurrentRegion() {
        $getGeoIP = self::getGeoIP();
        return $getGeoIP['geoplugin_regionName'];
    }

    /**
     * Get geo by IP
     * @return type
     */
    public static function getGeoIP() {
        return unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.self::getIP()));
    }

    /**
     * Get geo by IP
     * @return type
     */
    public static function getIP() {
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Get full url
     * @param type $use_forwarded_host
     * @return type
     */
    public static function getFullUrl() {
        return home_url($_SERVER['REQUEST_URI']);
    }

    /**
     * Get API Center license key
     * @return type
     */
    public static function getLicenseKey() {
        return get_option('ga4_tracking_api_center_license_key', true);
    }

    /**
     * Get API Center token
     * @return type
     */
    public static function getToken() {
        return self::getConfigOption('api_center_token');
    }

    /**
     * Get token
     * @return type
     */
    public static function getTokenStatus() {
        return get_option('ga4_tracking_api_center_token_status');
    }

    /**
     * Get token
     * @return type
     */
    public static function token_enabled() {
        return self::getTokenStatus() == 'enabled';
    }
    

    /**
     * Check token
     * @return boolean
     */
    public static function checkToken() {
        $result = self::APIToken();
        if ($result['success'] ?? false) {
            if (self::getTokenStatus() != 'enabled') {
                update_option('ga4_tracking_api_center_token_status', 'enabled');
            }
            return 'enabled';
        } else {
            update_option('ga4_tracking_api_center_token_status', 'disabled');
            return 'disabled';
        }
    }

    /**
     * API token
     * @return type
     */
    private static function APIToken() {
        //url-ify the data for the POST
        $fields = ['token' => self::getLicenseKey()];
        $fields_string = json_encode($fields);

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $authorization = "Authorization: Bearer ".self::getToken(); // Prepare the authorisation token
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch,CURLOPT_URL, self::getConfigOption('api_center_url').'anonymousId');

        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

        //So that curl_exec returns the contents of the cURL; rather than echoing it
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

        //execute post
        $request = curl_exec($ch);
        curl_close($ch);

        return json_decode($request, true);
    }

    /**
     * Get anonymousId
     * @return string
     */
    public static function getAnonymousId($_anonymousId = '') {
        $anonymousId = $_COOKIE['anonymousId'] ?? '';
        if (empty($anonymousId)) {
            if (empty($_anonymousId)) {
                $anonymousId = self::generateAnonymousId();
            } else {
                $anonymousId = $_anonymousId;
            }
        }
        if (!empty($anonymousId) && $anonymousId != 'false') {
            setcookie('anonymousId', $anonymousId, time()+3600*24*365, '/', self::get_domain());
        }
        return $anonymousId;
    }

    /**
     * Generate anonymousId
     * @return boolean
     */
    private static function generateAnonymousId() {
        $result = self::APIToken();
        if ($result['success']) {
            if (self::getTokenStatus() != 'enabled') {
                update_option('ga4_tracking_api_center_token_status', 'enabled');
            }
            return $result['data']['anonymousId'] ?? '';
        } else {
            update_option('ga4_tracking_api_center_token_status', 'disabled');
            return '';
        }
    }

    /**
     * Get current domain
     * @return type
     */
    public static function get_domain() {
        $url = self::getFullUrl();
        $domain = parse_url((strpos($url, '://') === FALSE ? 'http://' : '') . trim($url), PHP_URL_HOST);
        if (preg_match('/[a-z0-9][a-z0-9\-]{0,63}\.[a-z]{2,6}(\.[a-z]{1,2})?$/i', $domain, $match)) {
            return $match[0];
        }
    }

    /**
     * Save referer
     */
    public static function saveReferrer() {
        $referrer = $_SERVER['HTTP_REFERER'] ?? '';
        if (!empty($referrer)) {
            $referrer = str_replace('https://', '', $referrer);
            $referrer = str_replace('http://', '', $referrer);
            $referrer = preg_replace("#/$#", "", $referrer);
            if (strpos($referrer, home_url()) === false) {
                $csv = array_map('str_getcsv', file(plugin_dir_path(__FILE__) .'../../referrer_mapping.csv'));
                $key = array_search($referrer, array_column($csv, '2'));
                if ($key !== false) {
                    $medium = $csv[$key]['0'];
                    $source = $csv[$key]['1'];
                    $host = $csv[$key]['2'];
                    setcookie('referrer_medium', $medium, time()+3600*24*365, '/', self::get_domain());
                    setcookie('referrer_source', $source, time()+3600*24*365, '/', self::get_domain());
                    setcookie('referrer_host', $host, time()+3600*24*365, '/', self::get_domain());
                }
            }
        }
    }
}
