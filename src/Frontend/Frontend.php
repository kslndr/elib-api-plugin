<?php
namespace Api\Frontend;

use Api\Includes\Functions;
use Api\Services\AccessService as AccessService;
use Api\Services\SessionService as Session;
use Api\Services\TransportService as Transport;

use Api\Frontend\Controllers\StandardsController as Standards;

defined( 'ABSPATH' ) || exit;

/**
 * The main class for working with the frontend
 */
class Frontend {

    /**
     * Wp init
     */
    public function wp_init() {
      //  header("Access-Control-Allow-Origin: *");
    }

    

    public function getDataFromApi () {
        $req = '?'.strstr($_SERVER['REQUEST_URI'], '&');
        wp_redirect( home_url().'/publications-home/'.$req );
        exit;
    }

    
    public function formInst () {
        $uuid= Transport::getUuidApiDataPost();
        if ($uuid) Session::setSessionInst($uuid);
        wp_redirect( home_url()."/advanced-search/", 301 );
        //include __DIR__ . '/src/login_institutions/form_login_inst.php';
    }

    
    public function setIp() {
        $ip = $_POST['ip'];
        add_to_log('ip ', $ip);

        if (!isset($_SESSION)) session_start();
        $_SESSION['inst']['ip'] = $ip;
        $name = isset($_SESSION['inst']['name']) ? $_SESSION['inst']['name'] : "";
        $reload = isset($_SESSION['inst']['reload']) ? $_SESSION['inst']['reload'] : "";
        echo $ip." | ".$name." | ".$reload;
        
        wp_die();
    }


    public function gd_enqueue() {
        wp_enqueue_style( 'advanced-search-style', plugins_url('/../../assets/css/advanced-search-style.css',__FILE__ ), [], time() );
        wp_enqueue_style( 'main-style', plugins_url('/../../assets/css/main.css',__FILE__ ), [], time() );
        wp_enqueue_style( 'select2-flat-theme', plugins_url('/../../assets/css/select2-flat-theme.css',__FILE__ ), [], time() );
        wp_enqueue_style( 'form-login-inst', plugins_url('/../../assets/css/form-login.css',__FILE__ ), [], time() );

        wp_register_script( 'main', plugins_url('/../../assets/js/main.js',__FILE__ ), ["jquery"], time() );
        wp_enqueue_script( 'main' );

        wp_register_script( 'modification', plugins_url('/../../assets/js/refine-search-modification.js',__FILE__ ), ["jquery"], time() );
        wp_enqueue_script( 'modification' );

        wp_register_script( 'search-lists', plugins_url('/../../assets/js/search-lists.js',__FILE__ ), ["jquery"], time() );
        wp_enqueue_script( 'search-lists' );

        wp_enqueue_style( 'mc-calendar', plugins_url('/../../assets/css/mc-calendar.min.css',__FILE__ ), [], time() );
        wp_register_script( 'mc-calendar', plugins_url('/../../assets/js/mc-calendar.min.js',__FILE__ ), ["jquery"], time() );
        wp_enqueue_script( 'mc-calendar' );
        
    }

    // login form institutions
    public function fe_form_login_institutions() {
        ob_start();
        include __DIR__ . '/src/modals/e-form-login-institutions.php' ;
        include __DIR__ . '/src/modals/modal-login-inst.html.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    // simple search
    public function ttype_to_search() {
        ob_start();
        include __DIR__ . '/src/search/e-lib-form.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    public function ttype_to_search_bottom() {
        ob_start();
        include __DIR__ . '/src/search/e-lib-bottom.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    // advanced-search
    public function tpower_search_form() {
        ob_start();
        include __DIR__ . '/src/search/advanced-search-form.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    // refine-search
    public function elib_publications_home() {
        ob_start();
        include __DIR__ . '/src/search/publications-home-form.php' ;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    public function fe_library_page() {
        ob_start();
        include __DIR__ . '/src/documents/e-library-page.php' ;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    // jaes-journal
    public function fe_jaes_journal() {
        ob_start();
        include __DIR__ . '/src/journals/jaes-journal.php' ;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    public function fe_jaes_journal_online() {
        ob_start();
        include __DIR__ . '/src/journals/jaes-journal-online.php' ;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    public function fe_jaes_journal_top() {
        ob_start();
        include __DIR__ . '/src/journals/jaes-journal-top.php' ;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    // standards
    public function fe_shortcode_standards() {
        ob_start();
        Standards::standards_list();
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    public function add_to_log($title, $obj_m) {
        $file = plugin_dir_path( __FILE__ ) . '/plugin.log';
        $current = file_get_contents($file);
        $current .= date("Y-m-d h:i:sa") . " | " . $title . " | " . json_encode($obj_m) . "\n";
        file_put_contents($file, $current);
    }

    // public function do_rewrite_section(){
    // 	// add_rewrite_rule( 'journal-online/([^/]*)/([^/]*)/?', 'index.php?pagename=journal-online&jaes_vol=$matches[1]&jaes_num=$matches[2]', 'top' );
    // 	add_rewrite_rule( 'journal-online/([^/]*)/([^/]*)/?',  __DIR__ . '/src/journals/jaes-journal-online.php?vol=&num= ', 'top' );
    // 	
    // 	add_filter( 'query_vars', public function( $vars ){
    // 		$vars[] = 'vol';
    // 		$vars[] = 'num';
    // 		return $vars;
    // 	} );
    // }


    public function change_cart_item_name( $item_name,  $cart_item,  $cart_item_key ) {
        echo @$cart_item['title'];
        // echo " | ".$item_name." | ";
        // echo " | ".$cart_item_key;
        // echo " | ".$cart_item['product_id'];
        // echo " | ".$cart_item['doc_id'];
        // echo " | ".$cart_item['img_src'];

        add_to_log('add to cart cart_item', $cart_item);
    }

    public function wc_remove_quantity_field_from_cart( $return, $product ) {
        if ( is_cart() ) return true;
    }

    public function custom_product_image( $_product_img, $cart_item, $cart_item_key ) {
        $a      =   '<img src="'.$cart_item['img_src'].'" style="width: 150px;"/>';
        return $a;
    }

    public function custom_product_images_link( $_product, $cart_item, $cart_item_key ){
        return $cart_item['reffer'];
    }

    
    public function add_to_cart_callback(){

        $doc_id = intval( $_POST['doc_id'] );
        $product_id = intval( $_POST['product_id'] );
        $title  = $_POST['title'];
        $price  = $_POST['price'];
        $img_src  = $_POST['img_src'];
        $reffer = $_POST['reffer'];
        $duration = $_POST['duration'];

        // check doc_id already in the cart
        foreach ( WC()->cart->get_cart() as $cart_item ) { 
            if(  $doc_id == $cart_item['doc_id'] ){
                // echo "$doc_id - already in the cart";
                echo json_encode(['add_to_cart' => 'already in the cart']);
                wp_die();
                break; 
            }
        }

        $quantity = 1; 
        $variation_id = 0;
        $variation = [
            'pa_duration' => sanitize_text_field($duration),
        ];
        $cart_item_data = [
            'custom_price' => sanitize_text_field($price),
            'key' => md5($doc_id.date("Y-m-d h:i:sa")),
            'doc_id' => $doc_id,
            'title' => $title,
            'img_src' => $img_src,
            'reffer' => $reffer,
        ];

        $var = WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data );

        echo json_encode(['send_mail' => $send_mail, 'add_to_cart' => $var]);
        wp_die();
    }

    public function save_cart_item_key_as_custom_order_item_metadata( $item, $cart_item_key, $values, $order ) {
        // Save the cart item key as hidden order item meta data
        $item->update_meta_data( '_cart_item_key', $cart_item_key );
        $item->update_meta_data( '_title', $values['title'] );
        $item->update_meta_data( '_doc_id', $values['doc_id'] );
        $item->update_meta_data( '_price', $values['custom_price'] );

        add_to_log('checkout_create_order item', $item);
        add_to_log('checkout_create_order cart_item_key', $cart_item_key);
        add_to_log('checkout_create_order values', $values);

    }

    public function action__woocommerce_payment_complete( $order_id ){
        
        $data = get_cart_data_for_order ($order_id);

        $add_to_db = add_to_db ($data);
        add_to_log('action__woocommerce_payment_complete add_to_db', $add_to_db);

        $send_mail = send_mail ($order_id, $data);
        add_to_log('action__woocommerce_payment_complete send_mail', $send_mail);

        include_once(plugin_dir_path( __FILE__ ).'rhythm_processes_order.php');

        $transaction_number = get_post_meta( $order_id, '_transaction_id')[0];
        $order_dater = get_post_meta( $order_id, '_completed_date')[0];
        $order_total = get_post_meta( $order_id, '_order_total')[0];
        $payload = array(
            "id" => $data['access_code'],				// required
            "store_id" => "string",                 	// required
            "event_id" => "string",                 	// required
            "total" => $order_total,					// required
            "total_in_base_currency" => $order_total,	// required
            "status" => "awaiting payment",         	// required
            "line_items" => array(
            array(
                "product_type" => "string",               // required
                "product_id" => "string",                 // required
                "unit_price" => 0,                        // required
                "quantity_to_be_shipped" => 0,            // required
                "quantity_shipped" => 0,                  // required
                "quantity_cancelled" => 0,                // required
                "quantity_pending_return" => 0,           // required
                "type" => "item",                         // required
                "status" => "awaiting payment",           // required
                "order_line_item_id" => "string",         // required
                "balance_due" => 0,                       // required
                "amount_paid" => 0,                       // required
                "amount_to_pay_upon_creation" => 0,       // required
                "amount_pending_capture" => 0,            // required
                "amount_refunded" => 0,                   // required
                "total" => $order_total,				  // required
                "total_in_base_currency" => $order_total, // required
                "discounts" => 0,                         // required
                "taxes" => 0,                             // required
                "related_transactions" => array(
                array(
                    "type" => "payment",
                    "transaction_id" => $transaction_number,// required
                    "transaction_number" => 0,            	// required
                    "transaction_line_item_id" => "string", // required
                    "amount" => 0,                        	// required
                    "date" => $order_dater					// required
                )
                ),
            )
            ),
            "sys_created_at" => $order_dater,           	// required
            "sys_last_modified_at" => $order_dater,     	// required
            "sys_created_by_id" => "string",                // required
            "sys_last_modified_by_id" => "string",          // required

        );
        $send_orders_data_to_rhythm = send_orders_data_to_rhythm($payload);
        add_to_log('action__woocommerce_payment_complete send_orders_data_to_rhythm', $send_orders_data_to_rhythm);
    }

    public function get_cart_data_for_order ($order_id) {

        $order = wc_get_order( $order_id );
        add_to_log('get_cart_data_by_item_key order', $order);

        $res = [];

        foreach ( $order->get_items() as $item ){
            $cart_item_key = $item->get_meta( '_cart_item_key' );
            $doc_id 	= $item->get_meta( '_doc_id' );
            $title 		= $item->get_meta( '_title' );
            $price 		= $item->get_meta( '_price' );
            
            $res[] = [
                'cart_item_key' => $cart_item_key,
                'doc_id' 	=> $doc_id,
                'title' 	=> $title,
                'price' 	=> $price,

            ];

            add_to_log('get_cart_data_for_order cart_item_key', $cart_item_key);
            add_to_log('get_cart_data_for_order doc_id', $doc_id);
            add_to_log('get_cart_data_for_order title', $title);
            add_to_log('get_cart_data_for_order price', $price);
            add_to_log('get_cart_data_for_order res', json_encode($res));
        }

        $access_code = substr(preg_replace("/[^0-9]/", '', $cart_item_key), 0, 7);
        add_to_log('get_cart_data_for_order access_code', $access_code);

        return [
            'data' => $res,
            'access_code' => $access_code,
        ];
    }

    public function add_to_db ($data) {

        include_once(plugin_dir_path( __FILE__ ).'config.php');

        $access_code = $data['access_code'];

        $res = [];
        foreach ($data['data'] as $item) {
            $doc_id = $item['doc_id'];
            $data_request = Transport::getApiData("/api/v1/set_access_code/".$access_code."/".$doc_id."/".md5($access_code.$doc_id));
            $res[] = $data_request;

            // add_to_log('add_to_db url_api', $url_api);
            add_to_log('add_to_db request', json_encode($data_request));
        }
        return json_encode($res);	
    }

    public function send_mail ($order_id, $data) {

        $to = get_post_meta( $order_id, '_billing_email');
        $transaction_number = get_post_meta( $order_id, '_transaction_id')[0];
        $order_dater = get_post_meta( $order_id, '_completed_date')[0];
        $order_total = get_post_meta( $order_id, '_order_total')[0];

        $domen = explode('://', home_url())[1];

        $subject = 'AES Product Access Code';

        // $cart_item_key = $data['data'][0]['cart_item_key'];
        // $doc_id = $data['data'][0]['doc_id'];
        // $title = $data['data'][0]['title'];

        $access_code = $data['access_code'];
        add_to_log('send_mail cart_item_key', $cart_item_key);
        add_to_log('send_mail doc_id', $doc_id);
        add_to_log('send_mail title', $title);
        add_to_log('send_mail access_code', $access_code);

        include_once(plugin_dir_path( __FILE__ ).'config.php');

        $access_code = $data['access_code'];

        $each_text = '<table cellspacing="0" cellpadding="6" border="1" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;width:100%;font-family:"Helvetica Neue",Helvetica,Roboto,Arial,sans-serif"> ' .
        '<thead>' .
        '<tr>' .
            ' <th scope="col" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Product</th> ' .
            '<th scope="col" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Quantity</th> ' .
            '<th scope="col" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Price</th> ' .
        '</tr> ' .
        '</thead> ' .
        '<tbody> ';
        foreach ($data['data'] as $item) {
            $cart_item_key 	= $item['cart_item_key'];
            $doc_id 		= $item['doc_id'];
            $title 			= $item['title'];
            $price 			= $item['price'];
            // $each_text = $each_text . '<a href="' .  home_url() . '/e-library-page/?id=' . $doc_id . '&code=' . $access_code . '">' . $title . '</a>' . '<br> ' .
            // 'Quantity: ' . '1 x $' . $price . ' = $' . $price  . '<br> ';
            $each_text = $each_text . '<tr><td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">'.
            '<a href="' .  home_url() . '/e-library-page/?id=' . $doc_id . '&code=' . $access_code . '">' . $title . '</a>' . 	
            '</td>' .
            '<td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">'.
            '1 </td>' .
            '<td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">'.
                '<span><span>$</span>'.$price .'</span>' .		
            '</td></tr>';
        }

        $each_text = $each_text .'</tbody> <tfoot>' .
        '<tr>' .
        '	<th scope="row" colspan="2" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left;border-top-width:4px">Subtotal:</th>' .
        '	<td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left;border-top-width:4px"><span><span>$</span>'.$order_total.'</span></td>' .
        '</tr>' .
        '					<tr>' .
        '	<th scope="row" colspan="2" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Payment method:</th>' .
        '	<td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">PayPal</td>' .
        '</tr>' .
        '					<tr>' .
        '	<th scope="row" colspan="2" style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Total:</th>' .
        '	<td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"><span><span>$</span>'.$order_total.'</span></td>' .
        '</tr>' .
        '</tfoot></table>' ;

        $message = '<div style="max-width: 500px;">' .
                '<div style="background: #dfdfdf;"> <div style="background: #017667"> ' .
                '<p style="color: #fcfdc8; padding: 10px; text-align: center;"><b> Audio Engineering Society, Inc. E-Commerce Administration Server </b></p>' .
                '</div> <br> </div>' .
                '<h4><b> AES Online Order #' . $order_id .' </b></h4>' .
                '<div style="margin-left: 90px;">' .
                'Order Number: ' . 'AES-000001' . '<br> ' .
                'Transaction Number: ' . $transaction_number . '<br> ' .
                'Order Date: ' . $order_dater .'<br> ' .
                'Order Total: $' . $order_total . '<br> ' .
                '<br> ' .

                'Electronic Download: '.count($data['data']).' item' . '<br> ' .
                'Product Access Code: ' . $access_code . '<br> ' .
                // 'Product Access Code: ' . '<a href="' .  home_url() . '/e-library-page/?id=' . $doc_id . '&code=' . $access_code . '">' . $access_code . '</a>' . '<br> ' .
                '<br> ' .
                'Standart Delivery: ' . '0 item AES PDF JOURNAL ARTICLES (E-ELIB)' . '<br> ' .
                '<br> ' .
                '</div>' .

                $each_text .

                /*
                'Item: ' . $title . '<br> ' .
                'Quantity: ' . '1 x $' . $price . ' = $' . $price  . '<br> ' .		
                'Jaes Value: ' . '' . '<br> ' .
                */
                /*
                'Delivery: ' . 'File' . '<br> ' .
                '<br> ' .
                'SUBTOTAL: ' . '(File Delivery) $' . $order_total . '<br> ' .
                '<br> ' .
                */
                
                '<br> ' . 
                'BILL TO' . '<br> ' .
                '<br> ' . 
                get_post_meta( $order_id, '_billing_first_name')[0] . ' ' . get_post_meta( $order_id, '_billing_last_name')[0]  . '<br> ' .
                get_post_meta( $order_id, '_billing_address_1')[0]  . '<br> ' .
                get_post_meta( $order_id, '_billing_city')[0]  . '<br> ' .
                get_post_meta( $order_id, '_billing_state')[0]  . ' ' . get_post_meta( $order_id, '_billing_postcode')[0]  . '<br> ' .
                'ph: ' . get_post_meta( $order_id, '_billing_phone')[0]  . '<br> ' .
                'email: ' . get_post_meta( $order_id, '_billing_email')[0]  . '<br> ' .
                'member N: ' . ''  . '<br> ' .			
                '<br> ' . 
                'SHIP TO' . '<br> ' .
                '<br> ' . 
                get_post_meta( $order_id, '_billing_first_name')[0] . ' ' . get_post_meta( $order_id, '_billing_last_name')[0]  . '<br> ' .
                get_post_meta( $order_id, '_billing_address_1')[0]  . '<br> ' .
                get_post_meta( $order_id, '_billing_city')[0]  . '<br> ' .
                get_post_meta( $order_id, '_billing_state')[0]  . ' ' . get_post_meta( $order_id, '_billing_postcode')[0]  . '<br> ' .
                'ph: ' . get_post_meta( $order_id, '_billing_phone')[0]  . '<br> ' .
                '<div style="margin-left: 90px;">' .
                '<br> ' .
                'END Order Number: ' . 'AES-000001' . '<br> ' .
                '</div> ' .
                '</div> ';

        $headers = array('Content-Type: text/html; charset=UTF-8','From: AES <support@'.$domen.'>');

        $sent_message = wp_mail( $to, $subject, $message, $headers );

        return $sent_message;
    }

    
    public function set_mail_content_type(){
        return "text/html";
    }

}
