<!-- The Modal -->
<div id="id01" class="form-modal">

  <!-- Modal Content -->
  <form class="form-modal-content form-animate e-form-login-institutions" action="<?= esc_url( admin_url('admin-post.php') ); ?>" method="post">
  <input type="text" name="action" value="formInst" hidden/>

  <span onclick="document.getElementById('id01').style.display='none'"class="form-close" title="Close Modal">&times;</span>
    <div class="tile-form-inst">
      <!--<img src="img_avatar2.png" alt="Avatar" class="avatar">-->
      <h3 class="tile-form-inst"> Login Institutions </h3>
    </div>

    <?php 
      if (!isset($_SESSION)) session_start();
      if ( isset($_SESSION['inst']['uuid']) && $_SESSION['inst']['uuid'] != 'empty' ) {
        ?><div class="form-inst-container" style="background-color:#f1f1f1; margin-top: 20px;"><p> 
          in the current session you are registered as <h4><b><?= $_SESSION['inst']['name'] ?></b></h4>
          enter email and password only if you wish to log in with other registration data
        </p></div><?php
      }
     ?>

    <div class="form-inst-container" style="display: flex; flex-direction: column;">
      <label style="font-size: 1.2em;" for="uname"><b>Username</b></label>
      <input class="input-form-inst" type="text" placeholder="Enter Username" name="uname" required>

      <label style="font-size: 1.2em;" for="psw"><b>Password</b></label>
      <input class="input-form-inst" type="password" placeholder="Enter Password" name="psw" required>

      <button class="form-inst-button" type="submit">Login</button>
      <!--<label>
        <input type="checkbox" checked="checked" name="remember"> Remember me
      </label>-->
    </div>

    <div class="form-inst-container" style="background-color:#f1f1f1">
      <button class="form-inst-button" type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
      <span class="psw form-inst">Forgot <a href="#">password?</a></span>
    </div>
  </form>
</div>
