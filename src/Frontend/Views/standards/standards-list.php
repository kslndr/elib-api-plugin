<?php foreach ($standards as $standard) { ?>

    <div class="simple-search-container max-width-800">
	    <div class="elementor-col-100 block-populated-journal ">

            <div style="margin-top:25px; display: flex;" class="row">
                <div style="margin:10px; width: 100%;">
                    <h4><a href="<?=  $standard->href ?>" >
                        <span><?= $standard->name .": ".  $standard->title ?></span>
                    </a></h4>
                    <!--<hr class="solid" style=" width: 100%;">-->
                </div>
                <div style="margin:10px">
                    <p style="width: 60px; text-align: center;">
                        <?php if ( false ) { ?>
                            <a><img width="36" src="<?php echo plugins_url('/../../assets/img/Open_Access_logo.png', __FILE__); ?>" style="margin-bottom: 7px"></a><br>
                            <span><small><b style="line-height: 17px; display: block;">OPEN ACCESS</b></small></span>
                        <?php } ?>
                    </p>
                </div>
            </div>

            <div style="margin-top:10px; display: flex;" class="row">
                <div style="margin:10px; width: 100%;">
                    <p>Printing Date: <?=  $standard->date ?></p>
                    <p>Publication History: <?=  nl2br($standard->history) ?></p>
                </div>
            </div>

            <div style="margin-top:10px; display: flex;" class="row">
                <div style="margin:10px; width: 100%;">
                    <b>Abstract: </b><?= nl2br($standard->abstract) ?>
                </div>
            </div>

            <div style="margin-top:15px;">
                <?php if ($standard->href_f != 'NAN ') {  ?>
                    
                    <p style="margin-left: 10px;">
                        <i class="fas fa-file-pdf" style="font-size:30px;color:#5e5eed;margin-right: 5px;"></i>
                        <?php if ( $standard->href_f == '#onlyMembers') { ?> 
                            <a onclick="document.getElementById('onlyMembers').style.display='block'" href="#">downloadable PDF (Full)</a> 
                        <?php } else { ?>
                            <a data-toggle="modal" href="<?=  $standard->href_f ?>" ><span>downloadable PDF (Full) </span> </a>
                        <?php } ?>
                        (<?= $standard->fullfilesize ?>)
                        <?= $standard->price_str ?> 
                    </p>
                <?php } ?>
                <?php if ($standard->href_i != 'NAN ') {  ?>
                    <p style="margin-left: 10px;">
                        <i class="fas fa-file-pdf" style="font-size:30px;color:#5e5eed;margin-right: 5px;"></i>
                        <a data-toggle="modal" href="<?=  $standard->href_i ?>" ><span>Downloadable Preview </span> </a>
                        (<?= $standard->previewfilesize ?>)
                    </p>
                <?php } ?>
            </div>
        
        </div>
    </div>

<?php } ?>