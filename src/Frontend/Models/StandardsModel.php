<?php
namespace Api\Frontend\Models;

use Api\Services\ConvertService as ConvertService;
use Api\Services\AccessService as AccessService;
use Api\Services\TransportService as Transport;
use Api\Includes\Functions;

class StandardsModel
{
    public static function getData() 
    {
        $access_data = AccessService::getAccessDataStandards();
        $ip = AccessService::getIp();
        $standards = Transport::getApiData("/api/v1/standard/fetch/")->data;
        // $api = Functions::getConfigOption('api_url');
        $api = 'https://elibrary.aes2.org';

        foreach ($standards as $standard)  { 
            $id = $standard->id;

            $standard->href = '#';

            if (isset($_SESSION['inst']['name'])) $hash = md5($id.$_SESSION['inst']['name']);
            elseif (isset($ip)) $hash = md5($id.$ip);
            else $hash = md5($id."111.111.222.111");

            $standard->price_str = " $".$standard->member_price." AES Member | $".$standard->non_member_price." Non-Member"; 
            if ($access_data["ip"] == 'ok' || $access_data["accsees_rhythm"]) {
                $standard->href_f =  $api."/api/v1/standard/download/".$id."/f/".$hash;
            }
            else {
                $standard->href_f = '#onlyMembers';
            }
            $standard->href_i =  $api."/api/v1/standard/download/".$id."/i/".md5($id."111.111.222.111");

            $standard->fullfilesize = ConvertService::formatBytes($standard->fullfilesize);
            $standard->previewfilesize = ConvertService::formatBytes($standard->previewfilesize);
            
        }

        return $standards;
    }

}