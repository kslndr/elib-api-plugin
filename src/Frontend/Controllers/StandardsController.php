<?php
namespace Api\Frontend\Controllers;

use Api\Frontend\Models\StandardsModel as Model;

class StandardsController
{
    public static function standards_list() {
        $standards = Model::getData();
        include_once(dirname( __FILE__, 2 ).'/Views/modals/modal-login-inst.html.php');
        include_once(dirname( __FILE__, 2 ).'/Views/modals/modal-only-members.html.php');
        include_once(dirname( __FILE__, 2 ).'/Views/standards/standards-list.php');
    }
    
}