<?php

namespace Api\Services;

class TransportService 
{
    const API = "https://elibrary.aes2.org";

    /**
     * get data from api metod=GET
     */
    public static function getApiData($url) 
    {        
        $url_api = self::API.$url;
        $request = wp_remote_get( $url_api, array('sslverify' => FALSE) );

        return json_decode($request["body"]);
    }


    /**
     * get data from api metod=POST
     */
    public static function getUuidApiDataPost() 
    { 
        $token = '111';
        $api = str_replace("https", "http", self::API);
        $url = $api."/api/v1/login_inst";
        $data["uname"] = $_POST['uname'];
        $data["psw"] = $_POST['psw'];
        return self::runCurl($data, $url, $token);

    }

    public static function runCurl($data, $url, $token) {
       
        $data_json = json_encode($data);

        $ch = curl_init();
        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_URL, $url);
        $request = curl_exec($ch);
        curl_close($ch);

        $pos = strpos($request, "}{");
        if ($pos === false) {
            $res = json_decode($request);
        }
        else {
            $res = json_decode("{".explode("}{", $request)[1]);
        }
        // var_dump($request, $res, $res->uuid);
        return $res;
    }
    
}

    
