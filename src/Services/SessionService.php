<?php

namespace Api\Services;

class SessionService 
{

    public static function setSessionInst($uuid)
    {
        if (!isset($_SESSION)) session_start();

        $_SESSION['inst']['valid'] = true;
        $_SESSION['inst']['timeat'] = time();
        $_SESSION['inst']['uuid'] = $uuid->uuid;
        $_SESSION['inst']['name'] = $uuid->name;
    }

}