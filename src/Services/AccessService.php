<?php

namespace Api\Services;

use Api\Services\TransportService as Transport;

class AccessService 
{
    /**
     * access control institutions by id or name for journal
     */
    public static function getAccessData($ip, $id = 0, $check_id = true) 
    {        
        $acces_data = [];
        if ($check_id) {
           if (!isset($_SESSION)) session_start();
            $inst_name = @!is_null($_SESSION['inst']['uuid']) ? md5($_SESSION['inst']['name']) : "0";
            $request_data = Transport::getApiData("/api/v1/ip-check/".$ip."/".$id."/".$inst_name);
            foreach ($request_data as $k => $v) {
                $acces_data[$k] = $v;
            } 
        }
        $acces_data['accsees_rhythm'] = self::getRhytmAccess();

        return $acces_data;
    }

    /**
     * access control institutions by id or name for standards
     */
    public static function getAccessDataStandards() 
    {       
        $ip = self::getIp();
        $acces_data = [];
        if (!isset($_SESSION)) session_start();
        $inst_name = @!is_null($_SESSION['inst']['uuid']) ? md5($_SESSION['inst']['name']) : "0";
        $request_data = Transport::getApiData("/api/v1/ip-check-standard/".$ip."/".$inst_name);
        foreach ($request_data as $k => $v) {
            $acces_data[$k] = $v;
        } 
        $acces_data['accsees_rhythm'] = self::getRhytmAccess();

        return $acces_data;
    }


    public static function getIp() 
    { 
        if (!isset($_SESSION)) session_start();

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


    /**
     * access control by membership rhythm
     */
    private static function getRhytmAccess() {
        $accsees_rhythm = false;
        $user_id = get_current_user_id();
        $membership_rhythm = get_user_meta( $user_id, 'membership', true );
        if ( !empty($membership_rhythm)) {
            if ( @$membership_rhythm["Membership_Type"] == "Associate" ) { 
                $accsees_rhythm = true;
            }
        }
        
        return $accsees_rhythm;
    }


}

    
